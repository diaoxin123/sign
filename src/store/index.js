import Vue from 'vue'
import Vuex from 'vuex'
import mutations from './mutations'
import state from './state'

Vue.use(Vuex)
// 设置一个共用的默认数据

export default new Vuex.Store({
    state,
    mutations
})
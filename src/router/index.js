import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/page/Login/Login'
import Home from '@/page/Home/Home'
import InfoMent from '@/page/adminPage/infoMent/info'
import student from '@/page/adminPage/student/student'
import Statistics from '@/page/adminPage/Statistics/Statistics'
import dormitory from '@/page/adminPage/dormitory/dormitory'
import adminUser from '@/page/adminPage/adminUser/adminUser'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home,
      children:[
        {
          path: '/student',
          name: 'student',
          component: student
        },
        {
          path: '/InfoMent',
          name: 'InfoMent',
          component: InfoMent
        },
        {
          path: '/dormitory',
          name: 'dormitory',
          component: dormitory
        },
        {
          path: '/adminUser',
          name: 'adminUser',
          component: adminUser
        },
        {
          path: '/',
          name: 'Statistics',
          component: Statistics
        }
      ]
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    
  ]
})

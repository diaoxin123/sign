// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
// Vuex 共用数据存储
import store from './store'

import 'element-ui/lib/theme-chalk/index.css';
import 'styles/reset.css'//初始化样式文件
import 'styles/border.css'//解决移动端 一像素问题
import 'styles/iconfont.css'

import ElementUI from 'element-ui';
import echarts from 'echarts'

Vue.use(ElementUI);


Vue.prototype.$echarts = echarts 
Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})

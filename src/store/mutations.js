export default {
    // state为定义的公共数据  city属于 commit中传过来的数据
    changeUser (state,userObj){
        // 修改值
            state.Userid = userObj.Userid
            state.Nickname = userObj.Nickname
            
            if(userObj.authority){
                state.authority = true
            }else{
                state.authority = false
            }
              
        // h5存储
        try {
            localStorage.Userid =  userObj.Userid
            localStorage.Nickname = userObj.Nickname
        } catch (error) {
            
        }
        
    },
    manageChangeData(state,data){
       
        state.manageTableData = data;
    }
}
'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_HOST: '"http://203.3.80.132:3000"'
  
  // API_HOST: '"http://localhost:3000"'
})

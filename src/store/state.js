let defaultUserid = ''
let defaultNickname = ''
let defaultauthority = true
let manageTableData = []

try {
    if(localStorage.Userid){
        defaultNickname = localStorage.Nickname
        defaultUserid = localStorage.Userid
    }
} catch (error) {
    
}
export default  {
    Userid: defaultUserid,
    Nickname: defaultNickname,
    authority: defaultauthority,
    manageTableData:manageTableData
}